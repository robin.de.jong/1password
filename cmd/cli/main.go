package main

import (
	"1password/pkg/password"
	"context"
)

func main() {
	ctx := context.Background()
	password.NewPasswordManager(ctx)
}
